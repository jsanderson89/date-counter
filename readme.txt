Date Counter
Final Version


In my previous job, we recieved office referral data from local schools,
and graphed it by week and grading period. Originally, my coworkers would
count the data manually. I created this tool to quickly count and
organize this data.

Different schools had different grading periods, so a schedule file is
required to identify the beginning and end of each week.


Copyright Jesse Anderson, 2012-2014.