﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace DateChecker_Windowed_Version2
{
    class DataProcessor
    {
        /* devmethods
        public List<string> DevMethod1(string schedulePath, string dataPath)
        {
            DataLoader DataLoader1 = new DataLoader();
            List<string> myArray = DataLoader1.LoadSchedule(schedulePath);

            return myArray;
        }
        public DataTable DevBuildTable(string schedulePath, string dataPath)
        {
            DataTable compiledDataset = new DataTable();

            compiledDataset.Columns.Add("column 1");
            compiledDataset.Columns.Add("column 2");

            int number = 5;
            for (int counter = 0; counter <= number; counter++)
            {
                compiledDataset.Rows.Add(counter, counter);
            }


            return compiledDataset;
        }
        */

        public DataTable GetDataTable(string schedulePath, string dataPath)
        {
            //DataTable displayTable = new DataTable();
            DataLoader DataLoader1 = new DataLoader();
            ScheduleLoader ScheduleLoader1 = new ScheduleLoader();

            DataTable scheduleTable = ScheduleLoader1.LoadSchedule(schedulePath);
            List<DateTime> dataList = DataLoader1.LoadData(dataPath);
            DataTable filtered = FilterData(scheduleTable, dataList);
            DataTable final = GetFinalTable(scheduleTable, filtered);

            return final;
        }

        static DataTable FilterData(DataTable inputTable, List<DateTime> inputData)
        {
            DataTable filteredData = new DataTable();
            //int discardedDates = 0;

            //Create Datatable
            filteredData.Columns.Add("Period", typeof(int));
            filteredData.Columns.Add("Week", typeof(int));
            filteredData.Columns.Add("Ticket", typeof(DateTime));

            foreach (DateTime i in inputData)
            {
                foreach (DataRow row in inputTable.Rows)
                {
                    if (row.Field<DateTime>("Date 1") <= i && i <= row.Field<DateTime>("Date 2"))
                    {
                        filteredData.Rows.Add(row.Field<int>("Period"), row.Field<int>("Week Number"), i);
                    }

                    else
                    {
                        //Console.WriteLine("FilterData: Date does not fall into any established period: line '{0}' discarded", i);
                        //discardedDates++;
                    }
                }
            }

            //Console.WriteLine("FilterData: Number of invalid dates discarded: {0}", discardedDates);
            return filteredData;
        }

        static DataTable GetFinalTable(DataTable inputTable, DataTable inputData)
        {
            DataTable finalOutput = new DataTable();

            //LINQ Statements
            var periodQuery = from sData in inputTable.AsEnumerable()
                              orderby sData.Field<int>("Period") ascending
                              group sData by sData.Field<int>("Period") into weekGroup
                              let count = weekGroup.Count()
                              select new { PeriodValue = weekGroup.Key, WeekCount = count };

             var weekQuery = (from queryData in inputData.AsEnumerable()
                             select queryData.Field<int>("Week")).Max();

            var periodCountQuery = (from queryData in inputData.AsEnumerable()
                                    select queryData.Field<int>("Period")).Max();


            //Creates Spacer in column one
            finalOutput.Columns.Add("  ");

            //Creates column titles
            foreach (var i in periodQuery)
            {
                StringBuilder periodTitle = new StringBuilder();
                periodTitle.Append("Period ").Append(i.PeriodValue);
                finalOutput.Columns.Add(periodTitle.ToString());
            }

            for (var counter = 1; counter <= weekQuery; counter++)
            {
                List<object> currentRow = new List<object>();
                object[] rowHolderArray;
                DataRow rowToAdd;

                StringBuilder weekLabel = new StringBuilder();
                weekLabel.Append("Week ").Append(counter);
                currentRow.Add(weekLabel.ToString());

                for (var periodCount = 1; periodCount <= periodCountQuery; periodCount++)
                {
                    var tCount = (from QueryData in inputData.AsEnumerable()
                                  where QueryData.Field<int>("Period") == periodCount && QueryData.Field<int>("Week") == counter
                                  select QueryData).Count();
                    currentRow.Add(tCount);
                }

                rowHolderArray = currentRow.ToArray();

                rowToAdd = finalOutput.NewRow();
                rowToAdd.ItemArray = rowHolderArray;

                finalOutput.Rows.Add(rowToAdd);
            }

            return finalOutput;
        }
    }
}
