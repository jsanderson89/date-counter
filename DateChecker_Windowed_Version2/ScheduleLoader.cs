﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace DateChecker_Windowed_Version2
{
    struct DatePair
    {
        public DateTime dateOne;
        public DateTime dateTwo;
    }

    class ScheduleLoader
    {
        private string[] ReadFile(string filepath)
        {
            try
            {
                string[] lines = File.ReadAllLines(filepath);
                return lines;
            }

            catch (ArgumentException)
            {
                throw;
            }

            catch (DirectoryNotFoundException)
            {
                throw;
            }

            catch (FileNotFoundException)
            {
                throw;
            }


        }

        public DataTable LoadSchedule(string schedulePath)
        {
            string[] myArray = ReadFile(schedulePath);
            DataTable returnValues = FilterScheduleArray(myArray);
            if (ValidateTable(returnValues))
            {
                return returnValues;
            }

            else
            {
                throw new EmptyInputException("No valid data was found in Schedule Input file. Be sure the file was not empty, or that the wrong file was not selected.");
            }
            
        }

        private bool ValidateTable(DataTable input)
        {
            var periods = (from queryData in input.AsEnumerable()
                           select queryData.Field<int>("Period")).Count();
            var weeks = (from queryData in input.AsEnumerable()
                         select queryData.Field<int>("Week Number")).Count();
            var dateOnes = (from queryData in input.AsEnumerable()
                            select queryData.Field<DateTime>("Date 1")).Count();
            var dateTwos = (from queryData in input.AsEnumerable()
                            select queryData.Field<DateTime>("Date 2")).Count();

            if (periods != 0 && weeks != 0 && dateOnes != 0 && dateTwos != 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private DataTable FilterScheduleArray(string[] inputArray)
        {
            DataTable scheduleTable = new DataTable();
            int periodCounter = 1;
            int weekCounter = 1;

            //Build Data Table
            scheduleTable.Columns.Add("Period", typeof(int));
            scheduleTable.Columns.Add("Week Number", typeof(int));
            scheduleTable.Columns.Add("Date 1", typeof(DateTime));
            scheduleTable.Columns.Add("Date 2", typeof(DateTime));

            foreach (string i in inputArray)
            {
                if (i != "")
                {
                    if (ValidateScheduleStringFormat(i))
                    {
                        DatePair pair = ConvertToDatePair(i);
                        scheduleTable.Rows.Add(periodCounter, weekCounter, pair.dateOne, pair.dateTwo);
                        weekCounter++;
                    }
                }

                else
                {
                    periodCounter++;
                    weekCounter = 1;
                }
            }
            return scheduleTable;
        }

        private DatePair ConvertToDatePair(string i)
        {
            string[] datepair = i.Split('-');
            DatePair returnPair;

            DateTime.TryParse(datepair[0], out returnPair.dateOne);
            DateTime.TryParse(datepair[1], out returnPair.dateTwo);

            return returnPair;
        }

        private bool ValidateScheduleStringFormat(string i)
        {
            string[] datepair = i.Split('-');
            DateTime dateOne;
            DateTime dateTwo;

            if (datepair.Length == 2)
            {
                if (DateTime.TryParse(datepair[0], out dateOne) && DateTime.TryParse(datepair[1], out dateTwo))
                {
                    if (dateOne < dateTwo)
                    {
                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }

                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }
        }
    }

}
