﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DateChecker_Windowed_Version2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void loadScheduleButton_Click(object sender, EventArgs e)
        {
            loadButtonDisplay("Select Schedule File", textBox1);
        }

        private void loadDataButton_Click(object sender, EventArgs e)
        {
            loadButtonDisplay("Select Data File", textBox2);
        }

        private void countButton_Click(object sender, EventArgs e)
        {
            try
            {
                DataProcessor DataProcessor1 = new DataProcessor();
                dataGridView1.DataSource = DataProcessor1.GetDataTable(textBox1.Text, textBox2.Text);
            }

            #region exceptionHandling
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }

            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }

            catch (EmptyInputException ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
        }

        private void loadButtonDisplay(string requestMessage, TextBox targetTextBox)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Select Schedule File";
            openFileDialog1.Filter = "Text Files (.txt)|*.txt";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                targetTextBox.Text = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
            }
        }    

    }
}
