﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace DateChecker_Windowed_Version2
{
    class DataLoader
    {
        private string[] ReadFile(string filepath)
        {
            try
            {
                string[] lines = File.ReadAllLines(filepath);
                return lines;
            }

            catch (ArgumentException)
            {
                throw;
            }

            catch (DirectoryNotFoundException)
            {
                throw;
            }

            catch (FileNotFoundException)
            {
                throw;
            }


        }

        public List<DateTime> LoadData(string dataPath)
        {
            string[] myArray = ReadFile(dataPath);
            List<DateTime> returnValues = FilterDataArray(myArray);

            if (returnValues.Count > 0)
            {
                return returnValues;
            }

            else
            {
                throw new EmptyInputException("No valid data was found in Data Input file. Be sure the file was not empty, or that the wrong file was not selected.");
            }
            
        }

        private List<DateTime> FilterDataArray(string[] inputArray)
        {
            List<DateTime> output = new List<DateTime>();
            foreach (string i in inputArray)
            {
                if (ValidateDataStringFormat(i))
                {
                    output.Add(ConvertToDateTime(i));
                }
            }
            return output;
        }

        private DateTime ConvertToDateTime(string i)
        {
            DateTime returnDate;
            DateTime.TryParse(i, out returnDate);

            return returnDate;
        }

        private bool ValidateDataStringFormat(string i)
        {
            DateTime date;

            if (DateTime.TryParse(i, out date))
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }

    [Serializable]
    public class EmptyInputException : System.Exception
    {
        public EmptyInputException()
        {

        }

        public EmptyInputException(string message):base(message)
        {
            
        }
    }
}
